import 'bootstrap';
import 'slick-carousel/slick/slick';
import $ from 'jquery';

var toggleAffix = function(affixElement, scrollElement) {
    var offsetTop = affixElement.data('offset-top');
    var style = affixElement.data('class-affix');

    if (scrollElement.scrollTop() >= offsetTop) {
        affixElement.addClass(style);
        return;
    }

    affixElement.removeClass(style);
};

$(document).ready(function(){
    $('.galery-carousel').slick({
        lazyLoad: 'ondemand',
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 3
              }
            },
            {
              breakpoint: 480,
              settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '40px',
                slidesToShow: 1
              }
            }
          ]
      });
  });

$('[data-toggle="affix"]').each(function() {
    var el = $(this);

    $(window).on('scroll resize', function() {
        toggleAffix(el, $(this));
    });

    toggleAffix(el, $(window));
});

$('html a[href^="#"]').on('click', function(e) {
	if($(this).attr("href") != "#") {
		e.preventDefault();
		$('html, body').animate({
	        scrollTop: $(this.hash).offset().top
	    }, 'slow');
	}
});